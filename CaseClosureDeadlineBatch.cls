global class x7S_CaseClosureDeadlineBatch implements Database.Batchable<SObject>,Database.Stateful,Schedulable{
    List<Case> updatecaseList = new List<Case>();


    global Database.QueryLocator start(Database.BatchableContext context){
        String caseOpenStatus =x7S_RCCareHelper.CASE_STATUS_OPEN;

        String query='SELECT Id,Intake_Form_Submitted__c,Case_Closure_Deadline__c,(SELECT Id FROM Services__r)'
        + ' FROM Case'
        + ' WHERE Case_Closure_Deadline__c <= TODAY AND Status =: caseOpenStatus AND Intake_Form_Submitted__c=true';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Case> scope){
        System.debug('Success'+scope);

        List<ARC_Case_Services__c> services = new List<ARC_Case_Services__c>();
        Set<Id> serviceId= new Set<Id>(); 
        List<ARC_Case_Services__c> arcCaseService= new List<ARC_Case_Services__c>();
        Set<Id> caseIds = new  Set<Id>();





        for(Case caseRecord : scope){
            List<ARC_Case_Services__c> servicesRec = caseRecord.Services__r;
            services.addall(servicesRec);
        }
       
        for(ARC_Case_Services__c service : services){
            serviceId.add(service.Id);
        }
        System.debug('Success'+serviceId);
        for(ARC_Case_Services__c service : [SELECT Id, Status__c,Sub_Status__c,Case__r.Status,Case__c 
         FROM ARC_Case_Services__c WHERE Id IN: serviceId AND Status__c!=:x7S_RCCareHelper.SERVICE_STATUS]){
            System.debug('Success'+service.Case__c);

                service.Status__c=x7S_RCCareHelper.SERVICE_STATUS;
                service.Sub_Status__c='';
                caseIds.add(service.Case__c);

             arcCaseService.add(service);

        }
        
        for( Id caseId : caseIds){
            System.debug('Success'+caseId);

            Case cases = new Case();
            cases.Id= caseId ;
            cases.Status=x7S_RCCareHelper.CASE_STATUS_CLOSED;
            cases.Sub_Status__c = x7S_RCCareHelper.CASE_SUB_STATUS_DEADLINE_PASSED;
            updatecaseList.add(cases);
        }


        try{

            if(! arcCaseService.isEmpty()){
                update arcCaseService;

            }
            
        }
        catch(exception e){
            System.debug('Class [x7S_LTROTriggerHandler.rollupSummaryOnCase] Exception :'+e.getCause() +' Line No :' +e.getLineNumber() + e.getMessage());
        }
        


    }
    global void finish(Database.BatchableContext context){

        if(! updatecaseList.isEmpty()){
            update updatecaseList;

        }
        System.debug('Success updatecaseList'+updatecaseList);
    }
    global void execute(SchedulableContext SC) {
        Database.executebatch(new x7S_CaseClosureDeadlineBatch(),1);
    }
}